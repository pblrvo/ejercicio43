package aplicacion;

import dominio.Localidad;
import dominio.Municipio;
import dominio.Provincia;
import java.util.ArrayList;

public class Principal{

	public static void main(String[] args){
		Localidad alcorcon = new Localidad();
		alcorcon.setNumeroHabitantes(3000);
		alcorcon.setNombre("Alcorcon");
		
		Localidad alcobendas = new Localidad();
                alcobendas.setNumeroHabitantes(4300);
		alcobendas.setNombre("Alcobendas");
		
		Localidad getafe = new Localidad();
                getafe.setNumeroHabitantes(1200);
		getafe.setNombre("Getafe");

		Localidad leganes = new Localidad();
                leganes.setNumeroHabitantes(2500);
		leganes.setNombre("Leganes");

		Localidad fuenlabrada = new Localidad();
                fuenlabrada.setNumeroHabitantes(1500);
		fuenlabrada.setNombre("Fuenlabrada");

		Localidad mostoles = new Localidad();
                mostoles.setNumeroHabitantes(3200);
		mostoles.setNombre("Mostoles");

		Municipio valdebebas = new Municipio();
		valdebebas.setNombre("Valdebebas");
		valdebebas.coleccionLocalidades.add(alcorcon);
		valdebebas.coleccionLocalidades.add(alcobendas);
		valdebebas.coleccionLocalidades.add(getafe);

		Municipio aranjuez = new Municipio();
		aranjuez.setNombre("Aranjuez");
		aranjuez.coleccionLocalidades.add(leganes);
		aranjuez.coleccionLocalidades.add(fuenlabrada);
		aranjuez.coleccionLocalidades.add(mostoles);
		
		Provincia madrid = new Provincia();
		madrid.setNombre("Madrid");	
		madrid.coleccionMunicipios.add(valdebebas);
		madrid.coleccionMunicipios.add(aranjuez);
		
		System.out.println(madrid);
	}
}
