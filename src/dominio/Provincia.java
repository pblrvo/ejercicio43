package dominio;
import java.util.ArrayList;
import dominio.Municipio;

public class Provincia{
	private String nombre;
	private Municipio municipio;
	
	public String getNombre(){
		return nombre;
	}

	public void setNombre(String nombre){
		this.nombre = nombre;
	}

	public ArrayList<Municipio> coleccionMunicipios = new ArrayList<>();
	
	public int calcularNumeroHabitantes(){
		int suma = 0;
		for(int i = 0; i < coleccionMunicipios.size(); i++){ 
			System.out.println(coleccionMunicipios.get(i));
			int numeroHabitantes = coleccionMunicipios.get(i).calcularNumeroHabitantes();
			suma += numeroHabitantes;
	}	
		return suma;
	}

	public Municipio getMunicipio(){
		return  municipio;
	}

	public void setMunicipio(Municipio municipio){
		this.municipio = municipio;
	}

	
	public String toString(){
		return "Provincia: " + nombre + "\nHabitantes: " + calcularNumeroHabitantes();
	}
}
