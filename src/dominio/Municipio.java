package dominio;
import java.util.ArrayList;
import dominio.Localidad;

public class Municipio{
	private String nombre;
	private Localidad localidad;

	public String getNombre(){
		return nombre;
	}

	public void setNombre(String nombre){
		this.nombre = nombre;
	}
	
	public Localidad getLocalidad(){
		return localidad;
	}

	public void setLocalidad(Localidad localidad){
		this.localidad = localidad;
	}
	
		public ArrayList<Localidad> coleccionLocalidades = new ArrayList();
	
	public int calcularNumeroHabitantes(){
	int suma = 0;	
	for(int i = 0; i < coleccionLocalidades.size(); i++){ 
			 System.out.println(coleccionLocalidades.get(i));
			int numeroHabitantes = coleccionLocalidades.get(i).getNumeroHabitantes();
			suma = suma + numeroHabitantes;
	}
		return suma;
	}	
		
	public String toString(){
		return "\nMunicipio: " + nombre + "\nHabitantes: " + calcularNumeroHabitantes();
	}
}
