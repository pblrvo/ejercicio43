 package dominio;
public class Localidad{
	private String nombre;
	private int numeroHabitantes;
	 String getNombre(){
		return nombre;
	}

	public void setNombre(String nombre){
		this.nombre = nombre;
	}
	
	public int getNumeroHabitantes(){
		return numeroHabitantes;
	}
	
	public void setNumeroHabitantes(int numeroHabitantes){
		this.numeroHabitantes = numeroHabitantes;
	}

	public String toString(){
		return "\nLocalidad: " + nombre + "\nHabitantes: " + numeroHabitantes;
	}	
}

